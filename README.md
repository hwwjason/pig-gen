# pig-gen
为了小伙伴新建的项目护航、启航，更快入门pig微服务，更快入手pigx升级版。

码云pig直通 [https://gitee.com/log4j/pig](https://gitee.com/log4j/pig)

官网pig直通 [https://pig4cloud.com/](https://pig4cloud.com/)

#### 介绍
pig项目脚手架,采用maven是一个很好的代码构建工具，采用“约定优先于配置”的原则进行项目管理，在一个团队做开发的过程中，团队之间协作有自己的定义方式，每个人的结构定义风格也不尽相同，在这样的背景下我们有必要去定义一个统一的代码骨架供团队使用，有利于团队需要开始一个新项目的时候，可利用自定义骨架一键生成项目。

#### 安装教程

1. idea编译打包
```
maven clean package
```

2. 安装到本地maven 仓库(下载包的同级目录)
```
mvn install:install-file -Dfile=pig-gen-2.4.2.jar -DgroupId=com.pig4cloud.archetype -DartifactId=pig-gen -Dversion=2.4.2 -Dpackaging=jar
```

3. 命令行创建使用
```
mvn archetype:generate ^
       -DgroupId=com.pig4cloud ^
       -DartifactId=pig-demo ^
       -Dversion=1.0.0-SNAPSHOT ^
       -Dpackage=com.pig4cloud.pig.demo ^
       -DarchetypeGroupId=com.pig4cloud.archetype ^
       -DarchetypeArtifactId=pig-gen ^
       -DarchetypeVersion=2.4.2 ^
       -DarchetypeCatalog=local
```

win采用“ ^ ”换行，linux采用“ \ ”

参数说明

> -DgroupId组ID，创建项目的groupId

> -DartifactId： 创建项目的artifactId

> -Dpackage：   创建项目的包名在，注意结构不然spring 扫不到

> -DarchetypeGroupId：pig-gen的组ID，值不需要进行修改pig-gen

> -DarchetypeArtifactId：pig-gen的artifactId，值不需要进行改变


#### 使用说明

1. 直接使用doc/script/项目进行使用
2. 或者进行编译代码使用

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [https://pig4cloud.com](https://pig4cloud.com/)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
